#
# Cookbook Name:: cookbook-gitlab-runner
# Recipe:: runner_configure
#
# Copyright 2016, GitLab Inc.
#


template 'config.toml' do
  path node['cookbook-gitlab-runner']['config_path']
  source "config.toml.erb"
  owner 'root'
  group 'root'
  mode '0600'
  helpers QuoteHelper
  variables(
    global: node['cookbook-gitlab-runner']['global_config'],
    runners: node['cookbook-gitlab-runner']['runners']
  )
end
